﻿module.exports = function (grunt) {

    grunt.initConfig({

        less: {
            css: {
                //options: {cleancss: true},
                files: {
                    "css/css.css":"src/less/main.less"
                }
            }
        },
        watch: {
            Master: {
                files: ['src/less/*.less'],
                tasks: ['less:css'],
                options: {
                    livereload: true,
                }
            }
        }
    });

    //load tasks
    grunt.loadNpmTasks('grunt-contrib-less');//https://github.com/gruntjs/grunt-contrib-less
    grunt.loadNpmTasks('grunt-contrib-watch');//https://github.com/gruntjs/grunt-contrib-watch
    grunt.loadNpmTasks('grunt-notify');//growl notifications //https://github.com/dylang/grunt-notify

    //register tasks
    grunt.registerTask('default', ['watch']);
};